import java.util.*

object OX {
    var XO = arrayOf(
        arrayOf(" ", " ", " "),
        arrayOf(" ", " ", " "),
        arrayOf(" ", " ", " ")
    )
    var turn = "X"
    var Row = 0
    var Colum = 0
    var Round = 0

    @JvmStatic
    fun main(args: Array<String>) {
        Start()
        while (true) {
            table()


            inputXO()

            if (checkWin()) {
                break
            } else if (checkDraw()) {
                break
            }
            switchTurn()
        }
    }

    fun Start() {
        println("XO Game")
        println("")
    }

    fun table() {
        println(" " + " 1 " + "2" + " 3 ")
        for (i in 0..2) {
            print(i + 1)
            for (j in 0..2) {
                print("|" + XO[i][j])
            }
            print("|")
            println()
        }
    }

    fun inputXO() {
        val kb = Scanner(System.`in`)
        while (true) {
            println("Turn $turn")
            print("Please input position(Row,Colum) : ")
            try {
                val a = kb.next()
                val b = kb.next()
                Row = a.toInt()
                Colum = b.toInt()
                if (Row > 3 || Row < 0 || Row == 0 || Colum > 3 || Colum < 0 || Colum == 0) {
                    println("Row and Column must be number 1-3")
                    continue
                }
                Row = Row - 1
                Colum = Colum - 1
                if (XO[Row][Colum] !== " ") {
                    println("Row " + (Row + 1) + " and Column " + (Colum + 1) + " can't choose again")
                    continue
                }
                Round++
                XO[Row][Colum] = turn
                break
            } catch (a: Exception) {
                println("Row and Column must be number")
                continue
            }
        }
    }

    fun checkWin(): Boolean {
        var check = false
        for (i in XO.indices) {
            if (XO[i][0] === turn && XO[i][1] === turn && XO[i][2] === turn
            ) {
                check = true
            }
        }
        for (i in XO.indices) {
            if (XO[0][i] === turn && XO[1][i] === turn && XO[2][i] === turn
            ) {
                check = true
            }
        }

        if (XO[0][0] === turn && XO[1][1] === turn && XO[2][2] === turn
        ) {
            check = true
        }
        if (XO[0][2] === turn && XO[1][1] === turn && XO[2][0] === turn
        ) {
            check = true
        }
        if (check == true) {
            table()
            println("$turn WINNER!")
            return true
        }
        return false
    }

    fun checkDraw(): Boolean {
        if (Round == 9) {
            table()
            println("DRAW")
            return true
        }
        return false
    }

    fun switchTurn() {
        turn = if (turn === "X") {
            "O"
        } else {
            "X"
        }
    }
}